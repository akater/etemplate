# -*- coding: utf-8; mode: org-development-elisp; -*-
#+title: etemplate-eproject
#+subtitle: Part of the =etemplate= Emacs Lisp package
#+author: =#<PERSON akater A24961DE3ADD04E057ADCF4599555CE6F2E1B21D>=
#+property: header-args :tangle etemplate-eproject.el :lexical t
#+startup: nologdone show2levels
#+todo: TODO(t@) HOLD(h@/!) | DONE(d@)
#+todo: BUG(b@/!) | FIXED(x@)
#+todo: TEST(u) TEST-FAILED(f) | TEST-PASSED(p)
#+todo: DEPRECATED(r@) | OBSOLETE(o@)

This feature is conceptually external to ~etemplate~.  Project class is an entity external to ~etemplate~.  But Emacs does not define project classes at the moment and so we have to.

Because the word project is used in core Emacs and does not mean something similar enough, we say “eproject”.

* Meta
** Metadata
#+begin_src elisp :results none
;; Author: Dima Akater <nuclearspace@gmail.com>
;; Maintainer: Dima Akater <nuclearspace@gmail.com>
;; Keywords: projects

#+end_src

** Dependencies
#+begin_src elisp :results none
(eval-when-compile (require 'cl-lib))
(eval-when-compile (require 'eieio))
(eval-when-compile (require 'mmxx-macros))
(require 'eieio-base)
#+end_src

* projects
#+begin_src elisp :results none
(defvaralias 'etemplate-projects 'etemplate--eprojects)
#+end_src

#+begin_src elisp :results none
(defvar etemplate--eprojects nil)
#+end_src

#+begin_src elisp :tangle no :results none :eval never
(defclass etemplate--eprojects-db
  ((records :accessor org-project-records :type list)))
#+end_src

* Class ~eproject~
#+begin_src elisp :results none
(defclass etemplate--eproject (eieio-instance-tracker ;; eieio-persistent
                               )
  (;; (file :initform etemplate-project-default-persistence-file)
   ;; (file-header-line :initform ";; etemplate's registered projects")
   (tracking-symbol :initform 'etemplate-projects)
   (dir :initarg :dir :reader etemplate-eproject-dir :type string)
   (name :initarg :name :reader etempalte-eproject-name :type string))
  :documentation "Superclass for all eprojects.  Should be external to `etemplate' package.  Not meant to be instantiated for now.")
#+end_src

* directory
** Definition
#+begin_src elisp :results none
(defun eproject-directory (project)
  (with-slots (dir name) project
    (expand-file-name name dir)))
#+end_src

* all-project-classes
#+begin_src elisp :results none
(defvar eproject-all-classes nil)
#+end_src

* define-eproject-class
#+begin_src elisp :results none
(defmacro-mmxx define-eproject-class ( name superclasses
                                       slots &rest options-and-doc
                                       &gensym instantiable class)
  "Like defclass but always inherits from etemplate--eproject, and records the class in `eproject-all-classes'."
  (cl-check-type name (and symbol (not null) (not (eql t)) (not keyword)))
  ;; (cl-check-type superclasses (list-of symbol))
  (let ((instantiable-form
         (or (when-let ((list (memq :instantiable options-and-doc)))
               (prog1 (cadr list)
                 (setq options-and-doc
                       (org-plist-delete options-and-doc :instantiable))))
             t)))
    `(let ((,instantiable ,instantiable-form)
           (,class (defclass ,name (,@superclasses etemplate--eproject)
                     ,slots ,@options-and-doc)))
       (when ,instantiable
         (cl-pushnew ',name eproject-all-classes
                     :test #'eq))
       ,class)))
#+end_src

* class-read
#+begin_src elisp :results none
(defun eproject-class-read ()
  (if (null eproject-all-classes)
      (error "No instantiable project classes defined")
    (intern
     (completing-read "Project (class): "
                      eproject-all-classes nil t))))
#+end_src

* eproject-clean
** Notes
Useful on a general level due to (unfortunately) ubiquitousness of tangling.

** Definition
#+begin_src elisp :results none
(cl-defgeneric eproject-clean (eproject))
#+end_src

* new
** Prerequisites
*** read-project-name
**** Definition
#+begin_src elisp :results none
(defun eproject-read-project-name (project-class-name)
  (read-from-minibuffer
   (format "Name for new %s: " project-class-name)))
#+end_src

* Basic projects
** elisp
#+begin_src elisp :results none
(define-eproject-class elisp-eproject ()
  ()
  :instantiable nil
  :documentation "Superclass for all Elisp projects.")
#+end_src

#+begin_src elisp :results none
(cl-defmethod eproject-clean ((project elisp-eproject))
  (mapc #'delete-file
        (directory-files (eproject-directory project) t
                         (rx (or (seq ".elc")
                                 (seq "-autoloads.el")
                                 (seq string-start "site-gentoo.d"))
                             string-end)
                         t)))
#+end_src

#+begin_src elisp :results none
(define-eproject-class elisp-single-file-project (elisp-eproject)
  ()
  :documentation "Elisp project consisting of single source file, as described in Elisp info.")
#+end_src

#+begin_src elisp :results none
(define-eproject-class elisp-multi-file-project (elisp-eproject)
  ()
  :documentation "Elisp project consisting of multiple source files, as described in Elisp info.")
#+end_src

** lisp
#+begin_src elisp :results none
(define-eproject-class lisp-eproject ()
  ()
  :instantiable nil
  :documentation "Superclass for all Lisp projects.")
#+end_src

#+begin_src elisp :results none
(define-eproject-class lisp-asdf-eproject (lisp-eproject)
  ()
  :documentation "Lisp project defininig Lisp system(s) with ASDF.")
#+end_src

* TODO project-with-main-file
- State "TODO"       from              [2021-12-14 Tue 00:01] \\
  File to visit after project creation
* TODO project-with-README
- State "TODO"       from "TODO"       [2021-12-14 Tue 20:44] \\
  It should be ~template-with-README~ because this makes sense on template level alone.
- State "TODO"       from              [2021-12-13 Mon 23:14] \\
  So, should it be ~project-with-README~, or ~template-with-README~?
* TODO project-with-symlinked-README
- State "TODO"       from              [2021-12-13 Mon 23:14]
* TODO print-object
* TODO org-project-file-scheme
  - State "TODO"       from              [2020-02-28 Fri 16:09] \\
    org-project-file-scheme defines “structure” of a generic file in an org project, i.e. generic hierarchy.
