# -*- coding: utf-8; mode: org-development-elisp; -*-
#+title: etemplate
#+subtitle: Initialize projects using pre-written stubs
#+author: =#<PERSON akater A24961DE3ADD04E057ADCF4599555CE6F2E1B21D>=
#+property: header-args :tangle etemplate.el :lexical t
#+startup: nologdone show2levels
#+todo: TODO(t@) HOLD(h@/!) | DONE(d@)
#+todo: BUG(b@/!) | FIXED(x@)
#+todo: TEST(u) TEST-FAILED(f) | TEST-PASSED(p)
#+todo: DEPRECATED(r@) | OBSOLETE(o@)

~etemplate~ provides generic function ~etemplate-initialize~ so that initialization of projects from templates can be conveniently customized by writing methods for it.  ~etemplate~ also provides some classes since neither templates nor projects classes are defined in Emacs.

The idea is to leverage a custom method combination for this ~initialize~ function.

* Overview
** Usage
The package is still very much work in progress.  You may try using ~etemplate-initialize~ but I'm still trying to get a grip on what the proper method combination should be so it will probably change.

** Installation
*** Gentoo
Add the ebuild repository and install from there:
#+begin_src sh :dir /sudo::/ :tangle no :results none
eselect repository enable akater && \
emerge app-emacs/etemplate
#+end_src

*** Other systems
**** TODO Use package-vc (Emacs 29+)
- State "TODO"       from              [2023-06-27 Tue 12:45]
**** Install manually
Ensure the following dependencies are installed into your system-wide site-lisp dir:
- [[https://framagit.org/akater/elisp-mmxx-macros][mmxx-macros]] (only needed at build time)
- [[https://gitlab.com/akater/elisp-eieio-akater-extras][eieio-akater-extras]] (only needed at build time)

Clone ~etemplate~, switch to =release= branch.

~etemplate~ can be installed into system-wide directory =/usr/share/emacs/site-lisp= (or wherever your ~SITELISP~ envvar points to) with
#+begin_src sh :tangle no :results none
make && sudo make install
#+end_src
but it's best to define a package for your package manager to do this instead.

If your package manager can't do that, and you don't want to install with elevated permissions, do
#+begin_src sh :tangle no :results none
SITELISP=~/.config/emacs/lisp SITEETC=~/.config/emacs/etc make && \
DESTDIR=~/.config/emacs SITELISP=/lisp SITEETC=/etc make install
#+end_src
(you may simply eval this block with =C-c C-c=; it won't be informative if it fails but hopefully it'll work)

In any case, you'll have to add load-path to your config, as demonstrated in [[Suggested config]].

If you don't have a system-wide site-lisp directory where dependencies are found in versionless directories, you may evaluate [[elisp:(url-retrieve"https://framagit.org/akater/emacs-fakemake/-/raw/master/fakemake-make.org"(lambda(_ b &rest __)(eww-parse-headers)(let((rb (current-buffer))(start(point)))(with-current-buffer b(let((inhibit-read-only t))(erase-buffer)(insert-buffer-substring rb start)(goto-char(point-min))(eww-display-raw b'utf-8)(org-mode))(goto-char(org-babel-find-named-block"defun bootstrap-sitelisp"))(pop-to-buffer b)(recenter 1))))(list(get-buffer-create"https+org")))][this block]] and use the command defined there to create a symlink to org (and other packages that you use) in “site-lisp directory to fill” which you may specify.  That directory should be specified as ~SITELISP~ then.  If the link above is scary, find bootstrap-sitelisp manually [[https://framagit.org/akater/emacs-fakemake/-/raw/master/fakemake-make.org][here]].

*** Notes for contiributors
If you have a local version of repository, compile with something like
#+begin_example sh :tangle no :results none
ORG_LOCAL_SOURCES_DIRECTORY="/home/contributor/src/etemplate"
#+end_example

This will link definitions to your local Org files.

*** Tests
If you want to run tests for ~etemplate~, ensure the following dependency is installed:
- [[https://framagit.org/akater/org-run-tests][ORT]]

** Suggested config
#+begin_example elisp
(use-package etemplate
  ;; Note: on Gentoo, no need to specify load-path
  :load-path "/usr/share/emacs/site-lisp/etemplate"
  ;; or maybe
  ;; :load-path "~/.config/emacs/lisp/etemplate"
  :custom
  (etemplate-user-full-name "Mr. Lisper")
  (org-eproject-author-string "Mr. Lisper, O.R.G.")
  (org-eproject-default-persistence-file (expand-file-name
                                          "org-projects"
                                          user-emacs-directory))
  :mode (;; (org-eproject-default-persistence-file . emacs-lisp-mode)
         ("/org-projects$" . emacs-lisp-mode)))
#+end_example

* TODO streamline the process of adding project files to agenda
  - State "TODO"       from              [2020-02-19 Wed 20:27]
* Metadata
#+begin_src elisp :results none
;; Author: Dima Akater <nuclearspace@gmail.com>
;; Maintainer: Dima Akater <nuclearspace@gmail.com>
;; Keywords: org, projects

#+end_src

* Features
** Procedure that creates from templates
#+begin_src elisp :results none
(require 'etemplate-create)
#+end_src

** Some org projects
#+begin_src elisp :results none
(require 'org-eproject)
#+end_src
